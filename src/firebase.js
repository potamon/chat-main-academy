import firebase from 'firebase';
const config = {
    apiKey: "AIzaSyDhN4eDgKxzESH4bAIcprC3k8SVTY92NWI",
    authDomain: "chatmaina-5d5f1.firebaseapp.com",
    databaseURL: "https://chatmaina-5d5f1.firebaseio.com",
    projectId: "chatmaina-5d5f1",
    storageBucket: "chatmaina-5d5f1.appspot.com",
    messagingSenderId: "262954397889",
    appId: "1:262954397889:web:7632d6b3cbadd4b07ee440",
    measurementId: "G-Q9FSEJP5BD"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;